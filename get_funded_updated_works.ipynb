{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Get funded works\n",
    "\n",
    "Get a comprehensive set of works from a single funder updated in a certain period of time. By updates, we mean publication of a formal update notification, which might be a retraction, corrigendum, correction, or similar.\n",
    "\n",
    "To make sure we haven't missed anything, we use cursors to scroll through the results.\n",
    "\n",
    "Updates are collected using the updated which means that if you run contiguous dates you will get all data, however it means that you will get records for which ANY field was updated. To query by date of publication of the original work, use `from-published-date` and `until-published-date` in the query filter. It isn't possible to query by the date a publication notice was added."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from crapiquery import CrApiQuery # class to query the Crossref REST API, in a file with this notebook\n",
    "import os                         # used to check file paths\n",
    "import pandas as ps               # used to create and save tables\n",
    "import json"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# user-defined variables\n",
    "\n",
    "# which funder we interested in?\n",
    "funder_id = '10.13039/100000002'\n",
    "\n",
    "# check for updates between these two dates\n",
    "start = '2024-01-01'\n",
    "end = '2024-03-31'\n",
    "\n",
    "# email for polite querying\n",
    "my_email = 'email@domain.org'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_filename(filters, prefix, suffix):\n",
    "    ''' define a filename based on query filters '''\n",
    "\n",
    "    funder = filters['funder']\n",
    "    for char in ['/', '.']:\n",
    "        funder = funder.replace(char, '-')\n",
    "\n",
    "    return prefix + '_' + funder + '_' + start + '_' + end + '.' + suffix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# query parameters (see https://api.crossref.org/ for documentation)\n",
    "params = {\n",
    "    'mailto': my_email\n",
    "}\n",
    "filters = {\n",
    "    'from-update-date': start,\n",
    "    'until-update-date': end,\n",
    "    'has-update': 1,\n",
    "    'funder': funder_id\n",
    "}\n",
    "\n",
    "# additional parameters\n",
    "max_pages = 100\n",
    "max_rows_per_page = 1000\n",
    "\n",
    "# output filename\n",
    "items_fname = get_filename(filters, 'updates', 'json')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Make the queries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First, let's see how many results we're expecting\n",
    "test_params = params.copy()\n",
    "test_params['rows'] = 1\n",
    "\n",
    "test_query = CrApiQuery()\n",
    "js, status = test_query.get_works(\n",
    "    test_params, \n",
    "    filters\n",
    ")\n",
    "\n",
    "print(f\"{js['message']['total-results']} results in query\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# load the data, either from file or get it fresh from an API\n",
    "\n",
    "if (os.path.exists(items_fname)):\n",
    "    # load from existing file\n",
    "    with open(items_fname, 'r', encoding='utf-8') as f:\n",
    "        items = json.load(f)\n",
    "else:\n",
    "    # query the Crossref REST API\n",
    "    query = CrApiQuery()\n",
    "    items = query.multipage_query(\n",
    "        params,\n",
    "        filters,\n",
    "        max_pages,\n",
    "        max_rows_per_page\n",
    "    )\n",
    "\n",
    "    # save to file\n",
    "    with open(items_fname, 'w') as f:\n",
    "        json.dump(items, f, indent=2)\n",
    "    print(f\"data saved to {items_fname}\")\n",
    "\n",
    "print(f\"{len(items)} metadata records retrieved\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pull out the relevant funding data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# a few useful functions for this section\n",
    "\n",
    "def filter_for_funder(funding_metadata, funder):\n",
    "    ''' filter some funding metadata for a specific funder '''\n",
    "\n",
    "    # does it have a funding DOI?\n",
    "    funding_with_funder_ids = [x for x in funding_metadata if 'DOI' in x]\n",
    "    # is it the funding DOI we are looking for?\n",
    "    return [x for x in funding_with_funder_ids if x['DOI']==funder]\n",
    "\n",
    "\n",
    "def get_awards(item, funder):\n",
    "    ''' for an item, get a list of the awards for a funder of interest '''\n",
    "\n",
    "    # funding info for the funder we want\n",
    "    funding = filter_for_funder(item['funder'], funder)\n",
    "\n",
    "    # extract the awards\n",
    "    funding_with_awards = [f for f in funding if 'award' in f]\n",
    "    awards = [','.join(f['award']) for f in funding_with_awards]\n",
    "    return ','.join(awards)\n",
    "\n",
    "\n",
    "def get_funder_names(item, funder):\n",
    "\n",
    "    # funding info for the funder we want\n",
    "    funding = filter_for_funder(item['funder'], funder)\n",
    "\n",
    "    # extract funder names\n",
    "    funders = [f['name'] for f in funding]\n",
    "    return ','.join(funders)    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# put the data into a table format\n",
    "funder_table = []\n",
    "\n",
    "# go through each metadata record\n",
    "for item in items:\n",
    "\n",
    "    # get funder names and awards (grant IDs)\n",
    "    funder_names = get_funder_names(item, funder_id)\n",
    "    awards = get_awards(item, funder_id)   \n",
    "\n",
    "    # go through each update in the record\n",
    "    for update in item['updated-by']:\n",
    "        row = []\n",
    "        row.append(item['DOI'])    # work DOI\n",
    "        row.append(update['DOI'])  # update DOI\n",
    "        row.append(update['type']) # update type\n",
    "        row.append(awards)         # award IDs\n",
    "        row.append(funder_names)   # funder names\n",
    "\n",
    "    # add to the table\n",
    "    funder_table.append(row)  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for row in funder_table:\n",
    "    print(row)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "table_fname = get_filename(filters, 'table', 'csv')\n",
    "\n",
    "# turn into a pandas table\n",
    "titles = ['Updated work', 'update', 'update type', 'grant ID', 'funder name']\n",
    "funder_table_df = ps.DataFrame(funder_table, columns=titles)\n",
    "\n",
    "# save to file\n",
    "funder_table_df.to_csv(table_fname)\n",
    "print(f\"Table saved as {table_fname}\")\n",
    "\n",
    "funder_table_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
