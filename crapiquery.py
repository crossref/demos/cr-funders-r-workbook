''' 
query the Crossref REST API works endpoint
'''

import requests

WORKS_API_URL = 'https://api.crossref.org/v1/works/'

class CrApiQuery():
    ''' Functions to retrieve metadata from Crossref's rest API. '''

    def multipage_query(self, params:dict, filters:dict, max_pages:int, rows=1000, page_retry=3):
        '''
        Use pagination to get a complete set of results for a query.

        parameters
        ----------
        params:dict
            See https://api.crossref.org for allowed API parameters
        filters:dict
            See https://api.crossref.org for allowed filters for the works endpoint
        max_pages:int
            Maximum number of pages. Defined to stop the query going on forever
        rows:int
            Number of items per page, up to 1000. Recommended to use the maximum value unless
            experiencing tieouts.
        page_retry:
            How many times to attempt to retrieve each page before failing.

        '''

        # override any input parameters related to rows
        params['rows'] = rows
        # make sure we've got a cursor
        if 'cursor' not in params:
            params['cursor'] = '*'

        # list of items to return
        items = []

        # make the query for up to max_pages
        for page in range(max_pages):
            for retry in range(page_retry):
                # make query
                js, status = self.get_works(params, filters)

                # check success
                if (status>=400) & (status < 500):
                    # user error, will be printed by the get_works function
                    return {}
                if status != 200:
                    # otherwise try again
                    continue

                # save the results
                items += js['message']['items'].copy()

                # check completion
                if len(js['message']['items']) != rows:
                    # we've reached the last page!
                    return items

                # get ready for the next page
                params['cursor'] = js['message']['next-cursor']
                break

            if retry==page_retry-1:
                print(f"Stopping early! Failed to retrieve page {page}")
                break

        return items


    def get_works(self, params:dict, filters = None) -> tuple[dict, int]:
        '''
        Query the Crossref works endpoint and return json. In case of a problem returning
        the results, an empty dictionary is returned.

        See https://api.crossref.org for API documentation and information on available parameters

        parameters
        ----------
        params: dict
            query parameters for the Crossref works API,
            e.g. {'rows': 1, 'mailto':'myemail@demo.org'}
        filters: dict
            query filters e.g. {'from-pub-date': '2024-01-01', 'type': 'journal-article'}

        returns:
        dict:
            json output of the API as a dictionary
            the list of metadata records is in d['message']['items']
        int:
            the html status code of the query, e.g.
            200 for a successful query
            4XX for a user error
            5XX for a server error
        '''

        # reformat and add filters to the query parameters if used
        if isinstance(filters, dict):
            if len(filters.keys()) > 0:
                params['filter'] = self.filters_to_params(filters)

        # make the query
        r = requests.get(WORKS_API_URL, params=params, timeout=60)

        # get the json output from the API response
        if r.status_code == 200:
            js = r.json()
        else:
            # something went wrong, print the response and return an empty dictionary
            print(r.text)
            js = {}
        return js, r.status_code


    def filters_to_params(self, filters:dict) -> str:
        ''' Set the filters parameter for the Crossref works API using a dictionary

        parameters
        ----------
        filters: dict
            filters for the Crossref works endpoint, 
            e.g. {'from-pub-date': '2024-01-01', 'type': 'journal-article'}

        returns
        -------
        str:
            the filters expressed as a string, e.g. 'from-pub-date:2024-01-01,type:journal-article'
            This should be added to the query parameters dictionary with the key 'filter'

        '''

        # create an empty string for the filter values
        fval = ''
        # add each filter key and value to the string
        for f in filters:
            fval += str(f) + ':' + str(filters[f]) + ','
        fval = fval[:-1] # removing trailing comma

        return fval
