# Crossref API for funding data

Visit the notebook [here](https://crossref.gitlab.io/tutorials/cr-funders-r-workbook)

# Getting update metadata for funders

Funders would like to know when works that they have funded are updated or retracted. Here we provide several notebooks to show how to access that information.

## Notebooks

We have three notebooks that do the following:

### Get a single update

`demo_0_updates_for_funders.ipynb`

If you are new to the Crossref API and funding information, this takes you through through some API queries related to updates and shows where to look for useful information. 

In addition to the notebook, there is `demo_0_updates_for_funders.html` which is a slide deck running through the demonstration. It can be opened in any web browser.

### Demo for obtaining update information

`demo_1_funder_updates.ipynb`

Looks at to 1000 updates and the corresponding updated records. The output is a table containing the update DOIs, updated DOIs, funder name and ID, and grant IDs.

This is a demonstration of the kind of data that can be obtained and output. 

### Gathering updates for a single publisher in some timeframe

`get_funded_updated_works.ipynb`
`crapiquery.py`

The notebook and accompanying python script can be used to retrieve all updates for a funder in a given timeframe.
